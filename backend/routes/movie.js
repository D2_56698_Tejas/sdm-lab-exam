const {query} = require("express");
const express = require("express");
const router = express.Router();
const utils = require("../utils");

router.get("/:name", (request,response) =>{
    const { name } = request.param;

    const connection= utils.openConnection();

    const statement = `
         select * from Movie where
         name = '${name}'
    `;
    connection.query(statement, (error,result) => {
        connection.end();
        if(result.length > 0){
            response.send(utils.createResult(error,result));
        }else{
            response.send("movie not found .....");
        }
    });
});

router.post("/add", (request,response) =>{
    const { id,title, time, director_name } = request.body;

    const connection= utils.openConnection();

    const statement = `
         insert into Movie
            (movie_id,movie_title,movie_time,director_name)
        values
            ('${id}','${title}','${time}','${director_name}')
    `;
    connection.query(statement, (error,result) => {
        connection.end();
        response.send(utils.createResult(error, result));
    });
});

router.put("/update/:name", (request,response) =>{
    const { name } = request.param;
    const { time } = request.body;

    const connection= utils.openConnection();

    const statement = `
         update Movie
         set
         movie_time='${time}'
         where
         name = '${name}'   
    `;
    connection.query(statement, (error,result) => {
        connection.end();
        response.send(utils.createResult(error, result));
    });
});

router.delete("/remove/:name", (request,response) =>{
    const { name } = request.param;

    const connection= utils.openConnection();

    const statement = `
         delete from Movie
         where
            name = '${name}'   
    `;
    connection.query(statement, (error,result) => {
        connection.end();
        response.send(utils.createResult(error, result));
    });
});

module.exports = router;